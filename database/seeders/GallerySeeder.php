<?php

namespace Database\Seeders;

use App\Models\Gallery;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Gallery::count()==0){
            $gallery = [
                ["posisi"=>"center","image"=>'test.jpg'],
                ["posisi"=>"center","image"=>'test.jpg'],
                ["posisi"=>"center","image"=>'test.jpg'],
                ["posisi"=>"right","image"=>'test.jpg'],
                ["posisi"=>"right","image"=>'test.jpg'],
                ["posisi"=>"right","image"=>'test.jpg'],
                ["posisi"=>"left","image"=>'test.jpg'],
                ["posisi"=>"left","image"=>'test.jpg'],
                ["posisi"=>"left","image"=>'test.jpg'],
            ];
            Gallery::insert($gallery);
        }
       
    }
}
