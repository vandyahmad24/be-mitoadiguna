<?php

namespace Database\Seeders;

use App\Models\ClinicalApplication;
use App\Models\DetailClinicalApplication;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DetailClinicalApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('detail_clinical_applications')->truncate();
        $ca = ClinicalApplication::all();
        foreach($ca as $c){
            $data = [
                [
                    "title"=>"CONTAINS",
                    "body"=>"CONTAINS BODY",
                    "clinical_application_id"=>$c->id,
                ],
                [
                    "title"=>"PROCEDURES",
                    "body"=>"CONTAINS BODY",
                    "clinical_application_id"=>$c->id,
                ],
                [
                    "title"=>"INDICATIONS",
                    "body"=>"CONTAINS BODY",
                    "clinical_application_id"=>$c->id,
                ],
                [
                    "title"=>"CONTRAINDICATIONS",
                    "body"=>"CONTAINS BODY",
                    "clinical_application_id"=>$c->id,
                ],
                [
                    "title"=>"LASMIK LASER BIOREVITALISATION",
                    "body"=>"CONTAINS BODY",
                    "clinical_application_id"=>$c->id,
                ],
            ];
            DetailClinicalApplication::insert($data);
        }
    }
}
