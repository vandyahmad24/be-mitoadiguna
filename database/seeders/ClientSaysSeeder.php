<?php

namespace Database\Seeders;

use App\Models\ClientSays;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientSaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(ClientSays::count()==0){
            $client = [
                ["client_rating"=>5,"body"=>'Slate helps you see how many 
                more days you need to work to 
                reach your financial goal.',"name"=>"Regina Miles","photo"=>"client_says.jpg","title"=>"Designer"],
                ["client_rating"=>5,"body"=>'Slate helps you see how many 
                more days you need to work to 
                reach your financial goal.',"name"=>"Regina Miles","photo"=>"client_says.jpg","title"=>"Designer"],
                ["client_rating"=>5,"body"=>'Slate helps you see how many 
                more days you need to work to 
                reach your financial goal.',"name"=>"Regina Miles","photo"=>"client_says.jpg","title"=>"Designer"]
            ];
            ClientSays::insert($client);
        }
    }
}
