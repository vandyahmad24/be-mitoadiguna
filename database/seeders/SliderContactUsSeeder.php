<?php

namespace Database\Seeders;

use App\Models\SliderContactUs;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SliderContactUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SliderContactUs::create([
            'title' => 'Connect with Us',
            'subtitle' => 'Please feel free to contact us if you need any further information',
            'image' => 'image.jpg'
        ]);
    }
}
