<?php

namespace Database\Seeders;

use App\Models\ClinicalApplication;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClinicalApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('clinical_applications')->truncate();


        $data = [
            [
                'clinical_application'=>'Lasmik for Urology',
                'product'=>'lazmik01',
            ],
            [
                'clinical_application'=>'Lasmik for Physiotherapy',
                'product'=>'lazmik01',
            ],
            [
                'clinical_application'=>'Lasmik for Gynecology',
                'product'=>'lazmik01',
            ],
            [
                'clinical_application'=>'Lasmik for Acupuncture',
                'product'=>'lazmik01',
            ],
            [
                'clinical_application'=>'Lasmik for Intravenous Laser',
                'product'=>'lazmik01',
            ],
            [
                'clinical_application'=>'Lasmik for Revitalization',
                'product'=>'lazmik01',
            ],
            // lazmik 2
            [
                'clinical_application'=>'Lasmik for Urology',
                'product'=>'lazmik02',
            ],
            [
                'clinical_application'=>'Lasmik for Physiotherapy',
                'product'=>'lazmik02',
            ],
            [
                'clinical_application'=>'Lasmik for Gynecology',
                'product'=>'lazmik02',
            ],
            [
                'clinical_application'=>'Lasmik for Acupuncture',
                'product'=>'lazmik02',
            ],
            [
                'clinical_application'=>'Lasmik for Intravenous Laser',
                'product'=>'lazmik02',
            ],
            [
                'clinical_application'=>'Lasmik for Revitalization',
                'product'=>'lazmik02',
            ],
            // lazmik 3
            [
                'clinical_application'=>'Lasmik for Urology',
                'product'=>'lazmik03',
            ],
            [
                'clinical_application'=>'Lasmik for Physiotherapy',
                'product'=>'lazmik03',
            ],
            [
                'clinical_application'=>'Lasmik for Gynecology',
                'product'=>'lazmik03',
            ],
            [
                'clinical_application'=>'Lasmik for Acupuncture',
                'product'=>'lazmik03',
            ],
            [
                'clinical_application'=>'Lasmik for Intravenous Laser',
                'product'=>'lazmik03',
            ],
            [
                'clinical_application'=>'Lasmik for Revitalization',
                'product'=>'lazmik03',
            ],

            //ozonette
            [
                'clinical_application'=>'Lasmik for Urology',
                'product'=>'ozonette',
            ],
            [
                'clinical_application'=>'Lasmik for Physiotherapy',
                'product'=>'ozonette',
            ],
            [
                'clinical_application'=>'Lasmik for Gynecology',
                'product'=>'ozonette',
            ],
            [
                'clinical_application'=>'Lasmik for Acupuncture',
                'product'=>'ozonette',
            ],
            [
                'clinical_application'=>'Lasmik for Intravenous Laser',
                'product'=>'ozonette',
            ],
            [
                'clinical_application'=>'Lasmik for Revitalization',
                'product'=>'ozonette',
            ]

            
          
        ];
        ClinicalApplication::insert($data);
    }
}
