<?php

namespace Database\Seeders;

use App\Models\Slider;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SliderSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title'=>'Our Products based on research and evidences medicine',
                'subtitle'=>'Based on up-to-date Jouenals medicine (Always up to date for medical journals)',
                'image' => 'slider1.png'
            ],
            [
                'title'=>'Our Products based on research and evidences medicine',
                'subtitle'=>'Based on up-to-date Jouenals medicine (Always up to date for medical journals)',
                'image' => 'slider2.png'
            ],
            [
                'title'=>'Our Products based on research and evidences medicine',
                'subtitle'=>'Based on up-to-date Jouenals medicine (Always up to date for medical journals)',
                'image' => 'slider3.png'
            ],
        ];
        Slider::insert($data);

    }
}
