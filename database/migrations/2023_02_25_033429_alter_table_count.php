<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('counter_pengunjungs', function (Blueprint $table) {
            $table->string('user_agent')->nullable();
            $table->string('type_count')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('counter_pengunjungs', function (Blueprint $table) {
            $table->string('user_agent')->nullable();
            $table->string('type_count')->nullable();
        });
    }
};
