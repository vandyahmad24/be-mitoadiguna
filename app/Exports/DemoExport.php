<?php

namespace App\Exports;

use App\Models\Demo;
use Maatwebsite\Excel\Concerns\FromCollection;

class DemoExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Demo::all();
    }
}
