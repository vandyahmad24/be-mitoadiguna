<?php

namespace App\Exports;

use App\Models\RequestBookList;
use Maatwebsite\Excel\Concerns\FromCollection;

class RequestBookExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return RequestBookList::all();
    }
}
