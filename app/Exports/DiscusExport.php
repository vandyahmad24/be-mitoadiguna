<?php

namespace App\Exports;

use App\Models\DiscusUs;
use Maatwebsite\Excel\Concerns\FromCollection;

class DiscusExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DiscusUs::all();
    }
}
