<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CounterPengunjung extends Model
{
    use HasFactory;
    protected $fillable = ['counter','user_agent','type_count'];
}
