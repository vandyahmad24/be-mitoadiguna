<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instagram extends Model
{
    use HasFactory;
    protected $fillable = ['caption', 'image','created_post','profile_picture'];
    public function getProfilePictureAttribute($value)
    {
    return asset("upload/photo_profil") . "/" . $value;
    }
    public function getImageAttribute($value)
    {
    return asset("upload/ig") . "/" . $value;
    }
}
