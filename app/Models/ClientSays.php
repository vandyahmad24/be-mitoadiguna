<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientSays extends Model
{
    use HasFactory;
    protected $fillable = ['client_rating', 'body','name','title','photo'];
    public function getPhotoAttribute($value)
    {
    return asset("upload/client_says") . "/" . $value;
    }
}
