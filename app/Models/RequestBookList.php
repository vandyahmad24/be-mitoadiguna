<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestBookList extends Model
{
    protected $fillable = ['name', 'phone','email','job','book_id'];
    use HasFactory;
}
