<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    protected $fillable = ['name', 'email', 'phone','nama_klinik','alamat','tanggal','jenis_alat','jam'];
    use HasFactory;
}
