<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailClinicalApplication extends Model
{
    protected $fillable = ['title','type','body','clinical_application_id','attribute'];
    use HasFactory;
}
