<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscusUs extends Model
{
    protected $fillable = ['name', 'email','number','country','company_name','interested','message'];
    
    use HasFactory;
}
