<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClinicalApplication extends Model
{
    protected $fillable = ['clinical_application', 'product'];
    use HasFactory;
}
