<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPhoto extends Model
{
    protected $fillable = ['image', 'product_name'];
    use HasFactory;
    public function getImageAttribute($value)
    {
    return asset("upload/photo_product") . "/" . $value;
    }
}
