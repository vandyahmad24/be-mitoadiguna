<?php

namespace App\Helpers;

class ResponseHelper
{
    public static function success($data = null)
    {
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ]);
    }

    public static function error($message, $status = 400, $data = null)
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }
}