<?php

namespace App\Console\Commands;

use App\Http\Controllers\ApiInstagramController;
use Illuminate\Console\Command;

class ScrapIgCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap:ig';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'berfungsi untuk scrapping ig';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ApiInstagramController::updateIg();
        return Command::SUCCESS;
    }
}
