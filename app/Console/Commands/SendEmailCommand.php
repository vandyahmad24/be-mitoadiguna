<?php

namespace App\Console\Commands;

use App\Mail\NotifArtikel;
use App\Mail\NotifBookingEmail;
use App\Models\Post;
use App\Models\Subscribe;
use Illuminate\Console\Command;
use Mail;

class SendEmailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email {id}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command untuk mengirim email untuk subscriber';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $subscriber = Subscribe::all();
        $id = $this->argument('id');
        $post = Post::find($id);

        foreach($subscriber as $s){
            $mailData = [
                'title' => "Artikel terbaru dari Mito Adiguna",
                'body' => "Terdapat artikel terbaru dari Mito Adiguna dengan judul $post->title",
                'subbody'=>"",
                'url'=>"http://testingmito.booqable.online/#/news/".$post->id,
            ];
          
            // send to admin
            Mail::to($s->email)->send(new NotifArtikel($mailData));
        }
     
    }
}
