<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\PhotoProductRequest;
use App\Models\ProductPhoto;
use Illuminate\Http\Request;

class PhotoProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->query('product');
        $photo = ProductPhoto::where('product_name',$id)->orderBy('created_at','desc')->get();
        return view('dashboard.photo_product.index', compact('photo','id'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->query('product');
        return view('dashboard.photo_product.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoProductRequest $request)
    {
        $file = $request->file('image');
        $tujuan_upload = public_path('upload/photo_product');
        $fileName = time().'.'.$file->extension();  
        $file->move($tujuan_upload, $fileName);
        $id = $request->query('product');
        ProductPhoto::create([
            'image'=>$fileName,
            'product_name'=>$id
        ]);
        return redirect()->route('photo-product.index',['product'=>$id])->with('success', 'Sukses Menambahkan Photo Product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $product = $request->query('product');
        $photo = ProductPhoto::find($id);
        return view('dashboard.photo_product.edit', compact('product','photo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file = $request->file('image');
        $tujuan_upload = public_path('upload/photo_product');
        $fileName = time().'.'.$file->extension();  
        $file->move($tujuan_upload, $fileName);
        $product = $request->query('product');
        $photo = ProductPhoto::find($id);
        $photo->image = $fileName;
        $photo->save();
        return redirect()->route('photo-product.index',['product'=>$product])->with('success', 'Sukses Mengedit Photo Product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $product = $request->query('product');
        $photo =ProductPhoto::where('product_name',$product)->where('id',$id)->first();
        $photo->delete();
        
        return redirect()->route('photo-product.index',['product'=>$product])->with('success', 'Sukses Menghapus Photo Product');
    }

    public function listProduct($id)
    {
        $photo =ProductPhoto::where('product_name',$id)->get();
        return ResponseHelper::success($photo);
    }
}
