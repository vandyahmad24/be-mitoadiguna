<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\Instagram;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;

class ApiInstagramController extends Controller
{

    public function listInstagram(){
        $ig = Instagram::orderBy('created_at','desc')->limit(3)->get();
        return ResponseHelper::success($ig);
    }

    public static function updateIg(Request $request)
    {   
        if($request->key!="c09f128e-7653-4dcb-8fcb-5430af12c303"){
            return ResponseHelper::success("SUKSES");
        }

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://instagram-scraper2.p.rapidapi.com/medias_v2?user_id=12110122424&batch_size=4",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "X-RapidAPI-Host: instagram-scraper2.p.rapidapi.com",
                "X-RapidAPI-Key: 67e6b80e36msh3f56cbcb991c224p1053c2jsn749ecd41632a"
            ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return ResponseHelper::error($err);
        } else {
            $data = json_decode($response, true);
            foreach ($data['items'] as $item) {
                $imageUrl = null;
                $imageName=null;
                if (array_key_exists('image_versions2', $item)) {
                    $imageUrl = $item['image_versions2']['candidates'][0]['url'];
                } elseif (array_key_exists('carousel_media', $item)) {
                    $imageUrl = $item['carousel_media'][0]['image_versions2']['candidates'][0]['url'];
                }
                $profilName=null;
                if(array_key_exists('user',$item)){
                    $profilUrl =$item['user']['profile_pic_url'];
                    $timestamp = time();
                    $profilName = $timestamp . '.jpg';
                
                    $profilImage = file_get_contents($profilUrl);
                    $path = public_path('upload/photo_profil/' . $profilName);
                     file_put_contents($path, $profilImage);
                }

                if($imageUrl!=null){
                    $timestamp = time();
                    $imageName = $timestamp . '.jpg';
                
                    $profilImage = file_get_contents($imageUrl);
                    $path = public_path('upload/ig/' . $imageName);
                     file_put_contents($path, $profilImage);
                }


                $caption = $item['caption']['text'];
                $timestamp =  $item['caption']['created_at'];

                $datetime = Carbon::createFromTimestamp($timestamp)->toDateTimeString();

              
                // Simpan ke database menggunakan ORM Laravel
                $image = new Instagram;
                $image->image = $imageName;
                $image->caption = $caption;
                $image->created_post=$datetime;
                $image->profile_picture=$profilName;
                $image->save();
            }
            $ig = Instagram::orderBy('created_at','desc')->limit(3)->get();
            return ResponseHelper::success($ig);
        }
    }
    public function getImage($id)
    {
        $ig=Instagram::find($id);
        
        $imageData = file_get_contents($ig->image);
        if (strpos($ig->image, '/v/t50') !== false) {
            $content = 'video/mp4';
        } else {
            $content='image/jpeg';
        }
        $headers = [
            'Content-Type' => $content,
            'Cache-Control' => 'public, max-age=86400',
        ];

        return Response::make($imageData, 200, $headers);

    }
}
