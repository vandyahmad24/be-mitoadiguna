<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\Slider\SliderPostRequest;
use App\Http\Requests\Slider\SliderPutRequest;
use App\Models\Slider;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;

class SliderProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider = Slider::where('type','product')->orderBy('created_at', 'desc')->paginate(10);
        return view('dashboard.slider_product.slider', compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.slider_product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderPostRequest $request)
    {
        $file = $request->file('image');
        $tujuan_upload = public_path('upload/slider');
        $fileName = time().'.'.$file->extension();  
        $file->move($tujuan_upload, $fileName);

        $image = Image::make("upload/slider/$fileName");
        $image->encode('jpg', 75); // kompresi gambar dengan kualitas 75%
        $image->save("upload/slider/$fileName");


        Slider::create([
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'image' => $fileName,
            'type' =>'product',

        ]);
        return redirect()->route('slider-product.index')->with('success', 'Sukses Menambahkan slider');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('dashboard.slider_product.edit', ['slider'=>$slider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderPutRequest $request, $id)
    {
        
        $slider = Slider::find($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $tujuan_upload = public_path('upload/slider');
            $fileName = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileName);
            

            $image = Image::make("upload/slider/$fileName");
            $image->encode('jpg', 75); // kompresi gambar dengan kualitas 75%
            $image->save("upload/slider/$fileName");

            $slider->image = $fileName;

        }
        $slider->title=$request->title;
        $slider->subtitle=$request->subtitle;
        $slider->type='product';
        $slider->save();
        return redirect()->route('slider-product.index')->with('success', 'Sukses Mengedit slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::find($id);
        $slider->delete();
        return redirect()->route('slider-product.index')->with('success', 'Sukses Menghapus slider');
    }

    public function listSlider()
    {
        $slider = Slider::where('type','product')->orderBy('created_at', 'desc')->get();
        foreach($slider as $s){
            $s->image = asset("upload/slider")."/".$s->image;
        }
        return ResponseHelper::success($slider);
    }
}
