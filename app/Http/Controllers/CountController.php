<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CountController extends Controller
{
    public function grafik()
    {
        $startDate = now()->subMonth();
        $endDate = now();
        $dailyVisitors = DB::table('counter_pengunjungs')
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('SUM(counter) as total'))
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where('type_count','middleware')
            ->groupBy('date')
            ->get();
        return response()->json($dailyVisitors);

    }
}
