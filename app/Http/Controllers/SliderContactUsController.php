<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\Slider\SliderPostRequest;
use App\Http\Requests\Slider\SliderPutRequest;
use App\Models\SliderContactUs;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
class SliderContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider = SliderContactUs::orderBy('created_at', 'desc')->paginate(10);
       
        return view('dashboard.banner_contact_us.slider', compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.banner_contact_us.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderPostRequest $request)
    {
        $file = $request->file('image');
        $tujuan_upload = public_path('upload/slider-contact');
        $fileName = time().'.'.$file->extension();  
        $file->move($tujuan_upload, $fileName);

        $image = Image::make("upload/slider-contact/$fileName");
        $image->fit(1200, 800,function ($constraint) {
                    $constraint->aspectRatio();
                }); // mengubah ukuran gambar ke lebar dan tinggi maksimum 800px
                $image->encode('jpg', 75); // kompresi gambar dengan kualitas 75%
       
        $image->save("upload/slider-contact/$fileName");


        SliderContactUs::create([
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'image' => $fileName,
            'type'=>$request->type,
        ]);
        return redirect()->route('slider-contact-us.index')->with('success', 'Sukses Menambahkan slider');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = SliderContactUs::find($id);
       
        return view('dashboard.banner_contact_us.edit', ['slider'=>$slider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderPutRequest $request, $id)
    {
        
        
        $slider = SliderContactUs::find($id);
       
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $tujuan_upload = public_path('upload/slider-contact');
            $fileName = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileName);
            

            $image = Image::make("upload/slider-contact/$fileName");
            $image->fit(1200, 800,function ($constraint) {
                    $constraint->aspectRatio();
                }); // mengubah ukuran gambar ke lebar dan tinggi maksimum 800px
                $image->encode('jpg', 75); // kompresi gambar dengan kualitas 75%
           
            $image->save("upload/slider-contact/$fileName");

            $slider->image = $fileName;

        }

        
        $slider->title=$request->title;
        $slider->subtitle=$request->subtitle;
        // $slider->type=$request->type;
        $slider->save();

        return redirect()->route('slider-contact-us.index')->with('success', 'Sukses Mengedit slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = SliderContactUs::find($id);
        $slider->delete();
        return redirect()->route('slider-contact-us.index')->with('success', 'Sukses Menghapus slider');
    }

    public function listSlider()
    {
        $slider = SliderContactUs::first();
        $slider->image =asset("upload/slider-contact")."/".$slider->image;
    
        return ResponseHelper::success($slider);
    }

}
