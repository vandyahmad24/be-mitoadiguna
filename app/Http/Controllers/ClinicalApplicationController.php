<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\ClinicalApplication;
use App\Models\DetailClinicalApplication;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ClinicalApplicationController extends Controller
{
    public function index($product)
    {
        $ca = ClinicalApplication::where('product',$product)->paginate(10);
        return view('dashboard.clinical_application.index',compact('product','ca'));

    }
    public function detail($product,$id)
    {
        $ca = ClinicalApplication::find($id);
        $dca = DetailClinicalApplication::where('clinical_application_id',$id)->paginate(10);
        return view('dashboard.clinical_application.detail',compact('product','ca','dca'));

    }
    public function edit($product,$id,$detail_id)
    {
        $ca = ClinicalApplication::find($id);
        $dca = DetailClinicalApplication::find($detail_id);
        return view('dashboard.clinical_application.edit',compact('product','ca','dca'));

    }
    public function update(Request $request, $product,$id,$detail_id)
    {
       
        $fileName=null;
        $dca = DetailClinicalApplication::find($detail_id);
        
        $title = $dca->title;


        if($dca->title=="CONTAINS"){
            if ($request->hasFile('attribute')) {
                $file = $request->file('attribute');
                $tujuan_upload = public_path('upload/clinical_apps');
                $fileName = time().'.'.$file->extension();  
                $file->move($tujuan_upload, $fileName);
    
                $image = Image::make("upload/clinical_apps/$fileName");
                $image->fit(800, 800,function ($constraint) {
                    $constraint->aspectRatio();
                }); // mengubah ukuran gambar ke lebar dan tinggi maksimum 800px
                $image->encode('jpg', 75); // kompresi gambar dengan kualitas 75%
                $image->save("upload/clinical_apps/$fileName");
                $fileName = asset('upload/clinical_apps')."/".$fileName;
               
            }
        }

        if($dca->title == "LASMIK LASER BIOREVITALISATION" || $dca->title == "LASMIK LASER ILBI"){
            $fileName = $request->attribute;
        }
       

        $ca = ClinicalApplication::find($id);
        $listCa = ClinicalApplication::select('id')
                ->where('clinical_application',$ca->clinical_application)->get()->pluck('id')
                ->toArray();
           
        DetailClinicalApplication::whereIn('clinical_application_id', $listCa)->where('title',$dca->title)->update([
            'body'=>$request->body,
            'attribute'=>$fileName
        ]);
       
        return redirect()->route('clinical_application.detail',['product_id'=>$product,'id'=>$ca->id])->with('success', 'Sukses Mengubah Clinical App');

    }

    public function getListCA($productId)
    {
        $ca = ClinicalApplication::where('product', $productId)->get()->map(function ($item) {
            $dca = DetailClinicalApplication::where('clinical_application_id',$item->id)->where('title','!=','PROCEDURES')->get();
            $item['detail_clinical'] = $dca;
            return $item;
        });
        
        // dd($ca);
        
        return ResponseHelper::success($ca);
    }

    public function getListCAById($productId, $id)
    {
        $ca = ClinicalApplication::find($id);
        $dca = DetailClinicalApplication::where('clinical_application_id',$id)->where('title','!=','PROCEDURES')->get();
        $result=[
            "ClinicalApplication" => $ca,
            "DetailClinicalApplication"=>$dca
        ];
        return ResponseHelper::success($result);
    }



    
}



