<?php

namespace App\Http\Controllers;

use App\Exports\SubscribeExport;
use App\Helpers\ResponseHelper;
use App\Models\Subscribe;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SubscribeController extends Controller
{
    public function index()
    {
        $subscribe = Subscribe::orderBy('created_at','desc')->paginate(10);
        return view('dashboard.subscripe.index', compact('subscribe'));

    }
    public function post(Request $request)
    {
        if($request->email ==null){
            return ResponseHelper::error("email must be fill");
        }
        Subscribe::create([
            'email'=>$request->email
        ]);
        return ResponseHelper::success("success create subscribe");

    }

    public function export() 
    {
        return Excel::download(new SubscribeExport, 'subcribe-demo.xlsx');
    }
}
