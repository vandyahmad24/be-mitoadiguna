<?php

namespace App\Http\Controllers;

use App\Exports\RequestBookExport;
use App\Helpers\ResponseHelper;
use App\Mail\NotifReqBuku;
use App\Models\Book;
use App\Models\RequestBookList;
use Illuminate\Http\Request;
use Mail;
use Maatwebsite\Excel\Facades\Excel;

class RequestBookListController extends Controller
{
    public function create(Request $request)
    {
        $fields = ['name', 'phone', 'email', 'job', 'book_id'];

        foreach ($fields as $field) {
            if (!$request->has($field)) {
                return ResponseHelper::error("$field must be fill");
            }
        }

        $book = Book::find($request->book_id);
        if($book ==null){
            return ResponseHelper::error("book not found");
        }

        $reqBook=RequestBookList::create([
            'name'=>$request->name,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'job'=>$request->job,
            'book_id'=>$book->id,
        ]);

        $mailData = [
            'title' => "Link Download Buku ".$book->title,
            'body' => "Anda telah request buku dengan buku ".$book->title." berikut link downloadnya",
            'subbody'=>"",
            'url'=>$book->link,
        ];
      
        // send to admin
        Mail::to($reqBook->email)->send(new NotifReqBuku($mailData));

       return ResponseHelper::success($reqBook);

      
    }

    public function list()
    {
        $req = RequestBookList::latest()->paginate(10);
        return view('dashboard.req_book.index', compact('req'));

    }

    public function export() 
    {
        return Excel::download(new RequestBookExport, 'req-book-demo.xlsx');
    }
}
