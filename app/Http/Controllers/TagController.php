<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index()
    {
        $tag = Tag::orderBy('created_at','desc')->paginate(10);
        return view('dashboard.tag.index', compact('tag'));
    }
    
    public function create()
    {
        return view('dashboard.tag.create');
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
    
        Tag::create($request->all());
    
        return redirect()->route('tag.index')
            ->with('success', 'Tag created successfully.');
    }
    
    public function show(Tag $Tag)
    {
        // return view('tag.show', compact('Tag'));
    }
    
    public function edit(Tag $tag)
    {
        return view('dashboard.tag.edit', compact('tag'));
    }
    
    public function update(Request $request, Tag $tag)
    {
        $request->validate([
            'name' => 'required'
        ]);
    
        $tag->update($request->all());
    
        return redirect()->route('tag.index')
            ->with('success', 'Tag updated successfully.');
    }
    
    public function destroy(Tag $tag)
    {
        $tag->delete();
    
        return redirect()->route('tag.index')
            ->with('success', 'Tag deleted successfully.');
    }
    public function list()
    {
        $tag = Tag::orderBy('created_at','desc')->get();
        return ResponseHelper::success($tag);
    }
}
