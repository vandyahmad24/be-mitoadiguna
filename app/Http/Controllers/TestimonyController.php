<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\TestimoniPostRequest;
use App\Models\Testimony;
use Illuminate\Http\Request;

class TestimonyController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimoni = Testimony::orderBy('created_at', 'desc')->paginate(10);
        return view('dashboard.testimoni.testimoni', compact('testimoni'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.testimoni.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestimoniPostRequest $request)
    {
        
        $file = $request->file('image');
        $tujuan_upload = public_path('upload/testimoni');
        $fileName = time().'.'.$file->extension();  
        $file->move($tujuan_upload, $fileName);
        Testimony::create([
            'name' => $request->name,
            'role' => $request->role,
            'testimony' => $request->testimony,
            'image' => $fileName,
        ]);
        return redirect()->route('testimoni.index')->with('success', 'Sukses Menambahkan testimoni');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testimoni = Testimony::find($id);
        return view('dashboard.testimoni.edit', ['testimoni'=>$testimoni]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Testimony::find($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $tujuan_upload = public_path('upload/testimoni');
            $fileName = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileName);
            $slider->image = $fileName;
        }
        $slider->name=$request->name;
        $slider->role=$request->role;
        $slider->testimony=$request->testimony;
        $slider->save();
        return redirect()->route('testimoni.index')->with('success', 'Sukses Mengedit testimoni');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Testimony::find($id);
        $slider->delete();
        return redirect()->route('testimoni.index')->with('success', 'Sukses Menghapus testimoni');
    }

    public function listTestimoni()
    {
        $slider = Testimony::orderBy('created_at', 'desc')->get();
        foreach($slider as $s){
            $s->image = asset("upload/testimoni")."/".$s->image;
        }
        return ResponseHelper::success($slider);
    }
}
