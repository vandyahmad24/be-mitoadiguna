<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\TeamPostRequest;
use App\Models\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $team = Team::orderBy('created_at', 'desc')->paginate(10);
        return view('dashboard.team.team', compact('team'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamPostRequest $request)
    {
        
        $file = $request->file('image');
        $tujuan_upload = public_path('upload/team');
        $fileName = time().'.'.$file->extension();  
        $file->move($tujuan_upload, $fileName);
        Team::create([
            'name' => $request->name,
            'role' => $request->role,
            'image' => $fileName,
        ]);
        return redirect()->route('team.index')->with('success', 'Sukses Menambahkan team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::find($id);
        return view('dashboard.team.edit', ['team'=>$team]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Team::find($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $tujuan_upload = public_path('upload/team');
            $fileName = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileName);
            $slider->image = $fileName;
        }
        $slider->name=$request->name;
        $slider->role=$request->role;
        $slider->save();
        return redirect()->route('team.index')->with('success', 'Sukses Mengedit team');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Team::find($id);
        $slider->delete();
        return redirect()->route('team.index')->with('success', 'Sukses Menghapus team');
    }

    public function listTeam()
    {
        $slider = Team::orderBy('created_at', 'desc')->get();
        foreach($slider as $s){
            $s->image = asset("upload/team")."/".$s->image;
        }
        return ResponseHelper::success($slider);
    }
}
