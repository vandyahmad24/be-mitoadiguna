<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\ClientSays;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
class ClientSaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cs = ClientSays::orderBy('created_at','desc')->limit(3)->get();
        return view('dashboard.clientsays.index',compact('cs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cs = ClientSays::find($id);
        return view('dashboard.clientsays.edit',compact('cs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'client_rating' => 'required|numeric|min:0|max:5',
            'body' => 'required',
            'name' =>'required',
            'title' => 'required'
        ]);
        
        $client = ClientSays::find($id);
        if ($request->hasFile('photo')) {
           
            $file = $request->file('photo');
            $tujuan_upload = public_path('upload/client_says');
            $fileName = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileName);

            $image = Image::make("upload/client_says/$fileName");
            $image->fit(150, 150,function ($constraint) {
                $constraint->aspectRatio();
            }); // mengubah ukuran gambar ke lebar dan tinggi maksimum 800px
            
            $image->encode('jpg', 75); // kompresi gambar dengan kualitas 75%
            $image->save("upload/client_says/$fileName");

            $client->photo=$fileName;
        }

        
        $client->client_rating = $request->client_rating;
        $client->body=$request->body;
        $client->name=$request->name;
        $client->title=$request->title;
        $client->save();
        return redirect()->route('client-says.index')->with('success', 'Sukses Mengedit Client Says');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function listClient()
    {
        $cs = ClientSays::orderBy('created_at','desc')->limit(3)->get();
        return ResponseHelper::success($cs);
    }
}
