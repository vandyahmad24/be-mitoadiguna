<?php

namespace App\Http\Controllers;

use App\Exports\DemoExport;
use App\Helpers\ResponseHelper;
use App\Models\Demo;
use Illuminate\Http\Request;
use Mail;
use App\Mail\NotifBookingEmail;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;


class DemoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $demo = Demo::orderBy('created_at','desc')->paginate(10);
        return view('dashboard.demo.demo', compact('demo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createApi(Request $request)
    {

     

        $data =Demo::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'nama_klinik'=>$request->nama_klinik,
            'alamat'=>$request->alamat,
            'tanggal'=>$request->tanggal,
            'jenis_alat'=>$request->jenis_alat,
            'jam'=>$request->jam
        ]);

        Carbon::setLocale('id');

        // buat objek Carbon dari tanggal
        $date = Carbon::parse($data->tanggal);

        // tampilkan tanggal dalam format bahasa Indonesia
        $resultDate= $date->isoFormat('dddd, D MMMM YYYY');
       

        $mailData = [
            'title' => 'Terdapat Pendaftaran Booking Demo',
            'body' => "Terdapat Pendaftaran booking demo atas nama $data->name pada 
                        tanggal: $resultDate
                        jam:$data->jam
                        Jenis Alat:$data->jenis_alat
                        ",
            'subbody'=>"Silahkan hubungi Pemesan di nomor $data->phone, alamat pemesan $data->alamat",
            'url'=>route('booking-demo.index'),
        ];
       
      
        // send to admin
        Mail::to("freedemo@mitoadhiguna.co.id")->send(new NotifBookingEmail($mailData));

        //send to customer
        $mailData = [
            'title' => 'Pendaftaran Booking Demo',
            'body' => "Anda Melakukan Pendaftaran booking demo pada 
                        tanggal: $resultDate
                        jam:$data->jam
                        Jenis Alat:$data->jenis_alat
                        ",
            'subbody'=>"Silahkan menunggu admin menghubungi anda",
            'url'=>'',
        ];
        
      
         
        Mail::to($data->email)->send(new NotifBookingEmail($mailData));

        return ResponseHelper::success($data);
    }

    public function export() 
    {
        return Excel::download(new DemoExport, 'booking-demo.xlsx');
    }

}
