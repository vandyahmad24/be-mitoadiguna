<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\KeyValue;
use Illuminate\Http\Request;

class YoutubeController extends Controller
{
    public function index()
    {
        $yt = KeyValue::where('key','youtube')->first();
        return view('dashboard.youtube.index',compact('yt'));
    }
    public function update(Request $request)
    {
        $yt = KeyValue::where('key','youtube')->first();
        $embed_link = str_replace("https://youtu.be/", "https://youtube.com/embed/", $request->value);
        $yt->value=$embed_link;
        $yt->save();
        return redirect()->route('youtube.index')->with('success', 'Sukses Mengupdate url youtube');

    }

    public function getApi()
    {
        $yt = KeyValue::where('key','youtube')->first();
        return ResponseHelper::success($yt);
    }
}
