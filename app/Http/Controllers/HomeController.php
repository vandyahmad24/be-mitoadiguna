<?php

namespace App\Http\Controllers;

use App\Models\CounterPengunjung;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $today = Carbon::now()->toDateString();
        $month = Carbon::now()->month;

        $countToday = CounterPengunjung::where('type_count','middleware')->whereDate('created_at', $today)->count();
        $countMonth = CounterPengunjung::where('type_count','middleware')->whereMonth('created_at', $month)->count();


        $countArticleToday = CounterPengunjung::where('type_count','article')->whereDate('created_at', $today)->count();
        $countArticleMonth = CounterPengunjung::where('type_count','article')->whereMonth('created_at', $month)->count();


        return view('dashboard.home', compact('countToday','countMonth','countArticleToday','countArticleMonth'));
    }
}
