<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\BookPostRequest;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Book::orderBy('created_at', 'desc')->paginate(10);
        return view('dashboard.book.book', compact('book'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookPostRequest $request)
    {
        
        $file = $request->file('cover');
        $tujuan_upload = public_path('upload/book');
        $fileNameBook = time().'.'.$file->extension();  
        $file->move($tujuan_upload, $fileNameBook);


        $file = $request->file('link');
        $tujuan_upload = public_path('upload/book-link');
        $fileName = time().'.'.$file->extension();  
        $file->move($tujuan_upload, $fileName);

        Book::create([
            'judul' => $request->judul,
            'link' => asset('upload/book-link'). "/" . $fileName,
            'cover' => $fileNameBook,
        ]);
        return redirect()->route('book.index')->with('success', 'Sukses Menambahkan book');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        return view('dashboard.book.edit', ['book'=>$book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Book::find($id);
        if ($request->hasFile('cover')) {
            $file = $request->file('cover');
            $tujuan_upload = public_path('upload/book');
            $fileName = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileName);
            $slider->cover = $fileName;
        }

        if ($request->hasFile('link')) {
            $file = $request->file('link');
            $tujuan_upload = public_path('upload/book-link');
            $fileBook = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileBook);
            $slider->link = asset('upload/book-link'). "/" . $fileBook;
        }

        $slider->judul=$request->judul;
        $slider->save();
        return redirect()->route('book.index')->with('success', 'Sukses Mengedit book');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Book::find($id);
        $slider->delete();
        return redirect()->route('book.index')->with('success', 'Sukses Menghapus book');
    }

    public function listBook()
    {
        $slider = Book::orderBy('created_at', 'desc')->get();
        foreach($slider as $s){
            $s->image = asset("upload/book")."/".$s->cover;
        }
        return ResponseHelper::success($slider);
    }
}
