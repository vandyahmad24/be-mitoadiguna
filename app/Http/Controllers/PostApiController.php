<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\Post;
use Illuminate\Http\Request;

class PostApiController extends Controller
{
    public function listApi(Request $request)
    {
        $posts = Post::when($request->title, function ($query) use ($request) {
            return $query->where('title', 'like', "%{$request->title}%");
        })
        ->when($request->search, function ($query) use ($request) {
            return $query->where('title', 'like', "%{$request->search}%")
                         ->orWhere('body', 'like', "%{$request->search}%");
        })
        ->when($request->order, function ($query) use ($request) {
            if ($request->order == 'oldest') {
                return $query->oldest();
            }

            return $query->latest();
        }, function ($query) {
            return $query->latest();
        })
        ->when($request->status, function ($query) use ($request) {
          
            if ($request->status == 'published') {
                return $query->published();
            }

            return $query->drafted();
        })
        ->when($request->order, function ($query) use ($request) {
          
            if ($request->order == 'popular') {
                return $query->orderBy('count_view', 'desc');
            }
        })->paginate($request->get('limit', 9));
        foreach($posts as $p){
            $p->body=strip_tags($p->body);
        }


        return ResponseHelper::success($posts);
    }
    public function show($id)
    {
        $post = Post::find($id);
        if($post==null){
            return ResponseHelper::error("Post not found");
        }
        $post = $post->load(['category', 'comments.user', 'tags', 'user']);
        $description = strip_tags($post->body);
        $post->description = substr($description, 0, 200); 
        
        return ResponseHelper::success($post);
    }
}
