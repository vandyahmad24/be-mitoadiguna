<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\GalleryPostRequest;
use App\Models\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = Gallery::orderBy('created_at', 'desc')->paginate(10);
        return view('dashboard.gallery.gallery', compact('gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryPostRequest $request)
    {
        
        $file = $request->file('image');
        $tujuan_upload = public_path('upload/gallery');
        $fileName = time().'.'.$file->extension();  
        $file->move($tujuan_upload, $fileName);
        Gallery::create([
            'posisi' => $request->posisi,
            'image' => $fileName,
        ]);
        return redirect()->route('gallery.index')->with('success', 'Sukses Menambahkan gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = Gallery::find($id);
        return view('dashboard.gallery.edit', ['gallery'=>$gallery]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Gallery::find($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $tujuan_upload = public_path('upload/gallery');
            $fileName = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileName);
            $slider->image = $fileName;
        }
        $slider->posisi=$request->posisi;
        $slider->save();
        return redirect()->route('gallery.index')->with('success', 'Sukses Mengedit gallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Gallery::find($id);
        $slider->delete();
        return redirect()->route('gallery.index')->with('success', 'Sukses Menghapus gallery');
    }

    public function listGallery()
    {
        $slider = Gallery::orderBy('created_at', 'desc')->get();
        // inisialisasi array untuk setiap posisi
        $leftItems = array();
        $rightItems = array();
        $centerItems = array();
        
        foreach($slider as $s){
            $s->image = asset("upload/gallery")."/".$s->image;
            if ($s->posisi == 'left') {
                $leftItems[] = $s;
            } elseif ($s->posisi == 'right') {
                $rightItems[] = $s;
            } elseif ($s->posisi == 'center') {
                $centerItems[] = $s;
            }
        }
        $result = ["left"=>$leftItems, 
                    "right"=>$rightItems, 
                    "center"=>$centerItems];


        return ResponseHelper::success($result);
    }
}
