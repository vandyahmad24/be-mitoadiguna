<?php

namespace App\Http\Controllers;

use App\Exports\DiscusExport;
use App\Helpers\ResponseHelper;
use App\Http\Requests\DiscusUsRequest;
use App\Models\DiscusUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class DiscusUsController extends Controller
{
    public function index()
    {
        $discus = DiscusUs::orderBy('created_at','desc')->paginate(10);
        return view('dashboard.discus.index', compact('discus'));

    }
    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'number' => 'required',
            'country' => 'required',
            'company_name' => 'required',
            'interested' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            
            return ResponseHelper::error( $validator->errors());

        }
        


        DiscusUs::create([
            'name' => $request->name,
            'email'=> $request->email,
            'number'=> $request->number,
            'country'=> $request->country,
            'company_name'=> $request->company_name,
            'interested'=> $request->interested,
            'message'=> $request->message,
        ]);
        return ResponseHelper::success("success create DiscusUs");

    }

    public function export() 
    {
        return Excel::download(new DiscusExport, 'discus-demo.xlsx');
    }
}
