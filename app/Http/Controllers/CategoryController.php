<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
 public function index()
{
    $categories = Category::orderBy('created_at','desc')->paginate(10);
    return view('dashboard.categories.index', compact('categories'));
}

public function create()
{
    return view('dashboard.categories.create');
}

public function store(Request $request)
{
    $request->validate([
        'name' => 'required'
    ]);

    Category::create($request->all());

    return redirect()->route('categories.index')
        ->with('success', 'Category created successfully.');
}

public function show(Category $category)
{
    // return view('categories.show', compact('category'));
}

public function edit(Category $category)
{
    return view('dashboard.categories.edit', compact('category'));
}

public function update(Request $request, Category $category)
{
    $request->validate([
        'name' => 'required'
    ]);

    $category->update($request->all());

    return redirect()->route('categories.index')
        ->with('success', 'Category updated successfully.');
}

public function destroy(Category $category)
{
    $category->delete();

    return redirect()->route('categories.index')
        ->with('success', 'Category deleted successfully.');
}
public function list()
{
    $categories = Category::orderBy('created_at','desc')->paginate();
    return ResponseHelper::success($categories);
}
}
