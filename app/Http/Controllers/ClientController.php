<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\Client\ClientPostRequest;
use App\Models\Client;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = Client::orderBy('created_at', 'desc')->paginate(10);
        return view('dashboard.client.client', compact('client'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientPostRequest $request)
    {
        
        $file = $request->file('image');
        $tujuan_upload = public_path('upload/client');
        $fileName = time().'.'.$file->extension();  
        $file->move($tujuan_upload, $fileName);

        $image = Image::make("upload/client/$fileName");
        $image->fit(250,250,function ($constraint) {
            $constraint->aspectRatio();
        }); // mengubah ukuran gambar ke lebar dan tinggi maksimum 800px
        
        $image->encode('jpg', 75); // kompresi gambar dengan kualitas 75%
        $image->save("upload/client/$fileName");

        Client::create([
            'name' => $request->name,
            'image' => $fileName,
        ]);
        return redirect()->route('client.index')->with('success', 'Sukses Menambahkan Client');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        return view('dashboard.client.edit', ['client'=>$client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Client::find($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $tujuan_upload = public_path('upload/client');
            $fileName = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileName);

            $image = Image::make("upload/client/$fileName");
            $image->fit(250,250,function ($constraint) {
                $constraint->aspectRatio();
            }); // mengubah ukuran gambar ke lebar dan tinggi maksimum 800px
            
            $image->encode('jpg', 75); // kompresi gambar dengan kualitas 75%
            $image->save("upload/client/$fileName");

            $slider->image = $fileName;
        }
        $slider->name=$request->name;
        $slider->save();
        return redirect()->route('client.index')->with('success', 'Sukses Mengedit client');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Client::find($id);
        $slider->delete();
        return redirect()->route('client.index')->with('success', 'Sukses Menghapus Client');
    }

    public function listClient()
    {
        $slider = Client::orderBy('created_at', 'desc')->get();
        foreach($slider as $s){
            $s->image = asset("upload/client")."/".$s->image;
        }
        return ResponseHelper::success($slider);
    }
}
