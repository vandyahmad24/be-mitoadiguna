<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(['user', 'category', 'tags', 'comments'])->orderby('created_at','desc')->paginate(10);
        
        return view('dashboard.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id')->all();
        $tags = Tag::pluck('name', 'name')->all();

        return view('dashboard.posts.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $fileName ="";

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $tujuan_upload = public_path('upload/article');
            $fileName = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileName);

            $image = Image::make("upload/article/$fileName");
            $image->fit(1200, 800,function ($constraint) {
                $constraint->aspectRatio();
            }); // mengubah ukuran gambar ke lebar dan tinggi maksimum 800px
            
            $image->encode('jpg', 75); // kompresi gambar dengan kualitas 75%
            $image->save("upload/article/$fileName");
            $image->save("upload/thumbnail/$fileName");
        }
        


       
        $post = Post::create([
            'title'       => $request->title,
            'body'        => $request->body,
            'category_id' => 1,
            'image' => $fileName,
            'count_view' => 0,
            'thumbnail' => $fileName,
        ]);

        // $tagsId = collect($request->tags)->map(function ($tag) {
        //     return Tag::firstOrCreate(['name' => $tag])->id;
        // });

        // $post->tags()->attach($tagsId);
        // flash()->overlay('Post created successfully.');
        return redirect()->route('posts.index')->with('success', 'Sukses Menambahkan Posts');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post = $post->load(['user', 'category', 'tags', 'comments']);

        return view('dashboard.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
    
        $categories = Category::pluck('name', 'id')->all();
        $tags = Tag::pluck('name', 'name')->all();

        return view('dashboard.posts.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        // disbust cache

        

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $tujuan_upload = public_path('upload/article');
            $fileName = time().'.'.$file->extension();  
            $file->move($tujuan_upload, $fileName);
           
           
            $image = Image::make("upload/article/$fileName");
            $image->fit(1200, 800,function ($constraint) {
                $constraint->aspectRatio();
            }); // mengubah ukuran gambar ke lebar dan tinggi maksimum 800px
            $image->encode('jpg', 75); // kompresi gambar dengan kualitas 75%
            $image->save("upload/article/$fileName");
            $image->save("upload/thumbnail/$fileName");


            $post->image = $fileName;
            $post->thumbnail= $fileName;
            // dd($post);
        }

        $post->title=$request->title;
        $post->body=$request->body;
        // $post->category_id=$request->category_id;
        $post->save();

        // $tagsId = collect($request->tags)->map(function ($tag) {
        //     return Tag::firstOrCreate(['name' => $tag])->id;
        // });

        // $post->tags()->sync($tagsId);
        return redirect()->route('posts.index')->with('success', 'Sukses Mengedit Posts');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
      
        $post->delete();
        return redirect()->route('posts.index')->with('success', 'Sukses Menghapus Posts');
    }

    public function publish(Post $post)
    {
        $post->is_published = ! $post->is_published;
        $post->save();
        if($post->is_published==true){
            dispatch(function () use ($post) {
            $result = Artisan::call('send:email', [
                'id' => $post->id,
            ]);
            });
        }
        return redirect()->route('posts.index')->with('success', 'Sukses Publish Post');
    }
}
