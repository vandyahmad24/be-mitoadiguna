<?php

namespace App\Http\Middleware;

use App\Helpers\ResponseHelper;
use App\Models\CounterPengunjung;
use App\Models\Post;
use Closure;
use Illuminate\Http\Request;

class CountArticleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $user_agent = $request->header('User-Agent');
        if($user_agent==null){
            return ResponseHelper::error("User-Agent cannot be null");
        }

        $count = CounterPengunjung::where('user_agent',$user_agent)->where('type_count','article')->whereDate('created_at', now())->first();
       
        if($count==null){
            CounterPengunjung::create([
                'counter'=>1,
                'user_agent'=>$user_agent,
                'type_count'=>"article",
            ]);
        }
        $id = $request->route('id');
        $article = Post::find($id);
        if($article!=null){
            $article->count_view = $article->count_view +1;
            $article->save();
        }

        


        return $next($request);
    }
}
