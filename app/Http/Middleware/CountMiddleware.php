<?php

namespace App\Http\Middleware;

use App\Helpers\ResponseHelper;
use App\Models\CounterPengunjung;
use Closure;
use Illuminate\Http\Request;

class CountMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $user_agent = $request->header('User-Agent');
        if($user_agent==null){
            return ResponseHelper::error("User-Agent cannot be null");
        }

        $count = CounterPengunjung::where('user_agent',$user_agent)->where('type_count','middleware')->whereDate('created_at', now())->first();
       
        if($count==null){
            CounterPengunjung::create([
                'counter'=>1,
                'user_agent'=>$user_agent,
                'type_count'=>"middleware",
            ]);
        }

        

        return $next($request);
    }
}
