@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Menu Lazmik</strong></div>
            <div class="card-body">
               <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Menu</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Foto Product</td>
                            <td><a href="{{route('photo-product.index',['product'=>$id])}}" class="btn btn-primary">Link</a></td>
                        </tr>
                        {{-- <tr>
                            <td>2</td>
                            <td>Clinical application</td>
                            <td><a href="{{route('clinical_application.index',['product_id'=>$id])}}" class="btn btn-primary">Link</a></td>
                        </tr> --}}
                    </tbody>
                    
                  </table>
</div>

               

              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection