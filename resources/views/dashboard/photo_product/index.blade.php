@extends('template.dashboard_template')

@section('content')
@php
use Illuminate\Support\Str;
@endphp

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>List Photo Product {{$id}}</strong>
                        <nav style="--cui-breadcrumb-divider: '>';" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="{{route('product.menu',['id'=>$id])}}">{{$id}}</a></li>
                              <li class="breadcrumb-item active" aria-current="page">Photo Product</li>
                            </ol>
                          </nav>
                    </div>
                    <div class="card-body">
                        <div class="panel-heading">
                            <h2>
                                Photo
        
                                <a href="{{ route('photo-product.create',['product'=>$id]) }}" class="btn btn-primary pull-right">Buat Baru</a>
                            </h2>
                        </div>
                        <div class="example">
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Photo</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($photo as $p)
                                        <tr>
                                            <td> {{$loop->iteration}} </td>
                                            <td>
                                            <img src="{{$p->image}}" style='width:250px;height:auto;' border="0" alt="Null">
                                            </td>
                                            <td>
                                                <a href="{{route('photo-product.edit', ['photo_product' => $p->id, 'product' => $id])}}" class="btn btn-success">Edit</a>
                                                <form action="{{route('photo-product.destroy', ['photo_product' => $p->id, 'product' => $id])}}" method="POST" onsubmit="return confirm('Are you sure you want to delete this item?')">
                                                  @csrf
                                                  @method('DELETE')
                                                  <button type="submit" class="btn btn-danger">Delete</button>
                                              </form>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="5">No Photo available.</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
</div>

                            
            
                            </div>
                        </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>
   
@endsection
