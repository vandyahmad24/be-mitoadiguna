@extends('template.dashboard_template')

@section('content')

    <div class="body flex-grow-1 px-3">
        <div class="container-lg">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-header"><strong>Edit Artikel</strong></div>
                        <div class="card-body">
                            <div class="panel-body">
                                {!! Form::model($post, ['method' => 'PUT', 'url' => route('posts.update',$post->id), 'class' => 'form-horizontal', 'role' => 'form','enctype' => 'multipart/form-data']) !!}
        
                                    @include('dashboard.posts._form')

                                    <div class="mt-2">
                                        <label for="floatingPassword">Foto Artikel</label>
                                        <br>
                                        <img id="image-preview"  src="{{$post->image}}" style='width:250px;height:auto;' border="0" alt="Null">
                                    </div>

                                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Image</label>
                                        <input type="file" class="form-control" id="exampleInputPassword1" name="image" >
                                    
                                            <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <button type="submit" class="btn btn-primary">
                                                Update
                                            </button>
                                        </div>
                                    </div>
        
                                {!! Form::close() !!}
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
    
            </div>
        </div>
    </div>
@push('custom-scripts')
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
   CKEDITOR.replace('body', {
filebrowserUploadUrl: "{{ route('posts.upload', ['_token' => csrf_token() ]) }}",
filebrowserUploadMethod: 'form',
toolbar: [
    { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
		{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
		{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
		'/',
		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
		{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
		{ name: 'about', items: [ 'About' ] },
    ],
    allowedContent: true
});
</script>
@endpush
@push('custom-scripts')
<script>
$(document).ready(function() {
    // Ketika pengguna memilih file baru
    $('input[type="file"]').change(function() {
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          // Mengubah atribut 'src' pada elemen gambar untuk menampilkan gambar baru
          $('#image-preview').attr('src', e.target.result);
        }
        // Membaca file yang dipilih oleh pengguna
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
</script>
@endpush
@endsection
