@extends('template.dashboard_template')

@section('content')
@php
use Illuminate\Support\Str;
@endphp

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>List Artikel</strong></div>
                    <div class="card-body">
                        <div class="panel-heading">
                            <h2>
                                Posts
        
                                <a href="{{ route('posts.create') }}" class="btn btn-primary pull-right">Buat Baru</a>
                            </h2>
                        </div>
                        <div class="example">
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Body</th>
                                            <th>Author</th>
                                            {{-- <th>Category</th> --}}
                                            {{-- <th>Tags</th> --}}
                                            <th>Published</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($posts as $post)
                                        <tr>
                                            <td>{{ $post->title }}</td>
                                            <td>
                                                {{ Str::limit(strip_tags($post->body), $limit = 60, $end = '...') }}
                                            </td>
                                            <td>{{ $post->user->name ?? "" }}</td>
                                            {{-- <td>{{ $post->category->name ?? "" }}</td> --}}
                                            {{-- <td>{{ $post->tags->implode('name', ', ') }}</td> --}}
                                            <td>{{ $post->published ?? "" }}</td>
                                            <td>
            
                                                @php
                                                if($post->published == 'Yes') {
                                                $label = 'Draft';
                                                } else {
                                                $label = 'Publish';
                                                }
                                                @endphp
                                                <form action="{{ route('posts.publish', $post->id) }}" method="POST"
                                                    onsubmit="return confirm('Are you sure you want to Publish this artikel?')">
                                                    @csrf
                                                    @method('PUT')
                                                    <button type="submit" class="btn btn-xs btn-warning">{{ $label }}</button>
                                                </form>
            
                                                {{-- <a href="{{ url("/posts/{$post->id}") }}" class="btn btn-xs btn-success">Show</a> --}}
                                                <a href="{{ url("/posts/{$post->id}/edit") }}" class="btn btn-xs btn-info">Edit</a>
                                                <form action="{{ route('posts.destroy', $post->id) }}" method="POST"
                                                    onsubmit="return confirm('Are you sure you want to delete this item?')">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="5">No post available.</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
</div>

                                {{ $posts->links('vendor.pagination.bootstrap-4') }}
            
            
                            </div>
                        </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>
   
@endsection
