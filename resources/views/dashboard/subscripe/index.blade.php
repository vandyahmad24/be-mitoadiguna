@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Data Subscriber</strong></div>
            <div class="card-body">
              <a href="{{route('subscribe.export')}}" class="btn btn-primary">Download Data</a>
               <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Email</th>
                        <th scope="col">Subscribe At</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($subscribe as $s)
                        <tr>
                            <td>{{$loop->iteration + $subscribe->firstItem()-1}}</td>
                            <td>{{$s->email}}</td>
                            <td>{{ \Carbon\Carbon::parse($s->created_at)->format('d-m-Y') }}
                            </td>
                            
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $subscribe->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection