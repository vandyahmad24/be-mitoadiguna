@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>Tambah Buku</strong></div>
                    <div class="card-body">
                        <div class="example">
                            <form method="POST" action="{{route('book.store')}}" enctype='multipart/form-data'>
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }} <br/>
                                    @endforeach
                                </div>
                                @endif
                
                                @csrf
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="judul" id="floatingInput" placeholder="Judul" >
                                    <label for="floatingInput">Judul</label>
                                </div>
                                
                                <div class="mt-2">
                                    <label for="book" class="form-label">Upload Buku</label>
                                    <input type="file" class="form-control" id="book" name="link" required accept=".doc,.docx,.pdf">
                                </div>
                                
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Cover</label>
                                    <input type="file" id="cover" class="form-control" id="exampleInputPassword1" name="cover" required>
                                  </div>
                                  <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
