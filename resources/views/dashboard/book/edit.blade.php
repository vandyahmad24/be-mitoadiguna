@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>Edit Buku</strong></div>
                    <div class="card-body">
                        <div class="example">
                            <form method="POST" action="{{route('book.update',$book->id)}}" enctype='multipart/form-data'>
                               
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }} <br/>
                                    @endforeach
                                </div>
                                @endif
                
                                @csrf
                                @method("PUT")
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="judul" id="floatingInput" placeholder="Judul" value="{{$book->judul}}">
                                    <label for="floatingInput">Judul</label>
                                </div>
                                
                                <div class="mt-2">
                                    <label for="upload-book" class="form-label">Upload Buku</label>
                                    <input type="file" class="form-control" id="upload-book" name="link"  accept=".doc,.docx,.pdf">
                                </div>
                                
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Download Buku</label>
                                    <p><a href="{{$book->link}}">Download Link Sekarang</a></p>
                                </div>
                                
                                <div class="mt-2">
                                    <label for="floatingPassword">Cover Sekarang</label>
                                    <br>
                                    <img id="image-preview"  src="{{asset('upload/book/')}}/{{$book->cover}}" style='width:250px;height:auto;' border="0" alt="Null">
                                </div>
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Ganti Cover</label>
                                    <input type="file" id="cover" class="form-control" id="exampleInputPassword1" name="cover" >
                                  </div>

                                  <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@push('custom-scripts')
<script>
$(document).ready(function() {
    // Ketika pengguna memilih file baru
    $('#cover').change(function() {
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          // Mengubah atribut 'src' pada elemen gambar untuk menampilkan gambar baru
          $('#image-preview').attr('src', e.target.result);
        }
        // Membaca file yang dipilih oleh pengguna
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
</script>
@endpush

@endsection
