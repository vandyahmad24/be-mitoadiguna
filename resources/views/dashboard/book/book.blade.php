@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Data Buku/Jurnal</strong></div>
            <div class="card-body">
                <a href="{{route('book.create')}}" class="btn btn-primary">Tambah Buku/Jurnal</a>
              <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Link</th>
                        <th scope="col">Image</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($book as $s)
                        <tr>
                            <td>{{$loop->iteration + $book->firstItem()-1}}</td>
                            <td>{{$s->judul}}</td>
                            <td>{{$s->link}}</td>
                            <td> <img src="{{asset('upload/book/')}}/{{$s->cover}}" style='width:250px;height:auto;' border="0" alt="Null"></td>
                            <td>
                                <a href="{{route('book.edit',$s->id)}}" class="btn btn-success">Edit</a>
                                <form action="{{ route('book.destroy', $s->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this item?')">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-danger">Delete</button>
                              </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $book->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection