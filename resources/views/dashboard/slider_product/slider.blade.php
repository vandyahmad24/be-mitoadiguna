@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Data Slider</strong></div>
            <div class="card-body">
                <a href="{{route('slider-product.create')}}" class="btn btn-primary">Tambah Slider</a>
              <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Title</th>
                        <th scope="col">Sub Title</th>
                        <th scope="col">Image</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($slider as $s)
                        <tr>
                            <td>{{$loop->iteration + $slider->firstItem()-1}}</td>
                            <td>{{$s->title}}</td>
                            <td>{{$s->subtitle}}</td>
                            <td> <img src="{{asset('upload/slider/')}}/{{$s->image}}" style='width:250px;height:auto;' border="0" alt="Null"></td>
                            <td>
                                <a href="{{route('slider-product.edit',$s->id)}}" class="btn btn-success">Edit</a>
                                <form action="{{ route('slider-product.destroy', $s->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this item?')">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-danger">Delete</button>
                              </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $slider->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection