@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>Edit Team</strong></div>
                    <div class="card-body">
                        <div class="example">
                            <form method="POST" action="{{route('team.update',$team->id)}}" enctype='multipart/form-data'>
                               
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }} <br/>
                                    @endforeach
                                </div>
                                @endif
                
                                @csrf
                                @method("PUT")
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="name" id="floatingInput" placeholder="Nama" value="{{$team->name}}">
                                    <label for="floatingInput">Nama</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="role" id="floatingInput" placeholder="Role" value="{{$team->role}}">
                                    <label for="floatingInput">Role</label>
                                </div>
                                
                                <div class="mt-2">
                                    <label for="floatingPassword">Logo Sekarang</label>
                                    <br>
                                    <img id="image-preview"  src="{{asset('upload/team/')}}/{{$team->image}}" style='width:250px;height:auto;' border="0" alt="Null">
                                </div>
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Ganti Foto</label>
                                    <input type="file" class="form-control" id="exampleInputPassword1" name="image" >
                                  </div>

                                  <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@push('custom-scripts')
<script>
$(document).ready(function() {
    // Ketika pengguna memilih file baru
    $('input[type="file"]').change(function() {
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          // Mengubah atribut 'src' pada elemen gambar untuk menampilkan gambar baru
          $('#image-preview').attr('src', e.target.result);
        }
        // Membaca file yang dipilih oleh pengguna
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
</script>
@endpush

@endsection
