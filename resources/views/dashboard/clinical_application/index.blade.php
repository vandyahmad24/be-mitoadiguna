@extends('template.dashboard_template')

@section('content')
<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          
          <div class="card mb-4">
            <div class="card-header"><strong>Data Clinical Application</strong>
              
            </div>
            <div class="card-body">
               <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Name</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($ca as $s)
                        <tr>
                            <td>{{$loop->iteration + $ca->firstItem()-1}}</td>
                            <td>{{$s->clinical_application}}</td>
                            <td>
                                <a href="{{route('clinical_application.detail',['product_id'=>$product,'id'=>$s->id])}}" class="btn btn-primary">Detail</a>
                            </td>
                           
                            </td>
                            
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $ca->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection