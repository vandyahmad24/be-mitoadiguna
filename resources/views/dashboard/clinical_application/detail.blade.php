@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Data Clinical Application <b>{{$ca->clinical_application}}</b>   </strong>
               
            
            </div>
            <div class="card-body">
               <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Name</th>
                        <th scope="col">Body</th>
                        <th scope="col">Attribute</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($dca as $s)
                      @if($s->title=='PROCEDURES')
                      @php
                          continue;
                      @endphp
                      @endif
                        <tr>
                            <td>{{$loop->iteration + $dca->firstItem()-1}}</td>
                            <td>{{$s->title}}</td>
                            <td>{!!$s->body!!}</td>
                            <td>{{$s->attribute}}</td>
                            <td>
                                <a href="{{route('clinical_application.edit',['product_id'=>$product,'id'=>$ca->id,'detail_id'=>$s->id])}}" class="btn btn-primary">Detail</a>
                            </td>
                            
                           
                            </td>
                            
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $dca->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection