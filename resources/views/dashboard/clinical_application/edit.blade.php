@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>Edit Clinical Application {{$dca->title}} </strong>
                        
                    </div>
                    <div class="card-body">
                        <div class="example">
                            <form method="POST" action="{{route('clinical_application.edit',['product_id'=>$product,'id'=>$ca->id,'detail_id'=>$dca->id])}}" enctype='multipart/form-data'>
                               
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }} <br/>
                                    @endforeach
                                </div>
                                @endif
                
                                @csrf
                                @method("PUT")
                                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                    {!! Form::label('body', 'Body', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-12">
                                        {!! Form::textarea('body', $dca->body, ['class' => 'form-control', 'id'=>'body', 'required']) !!}
                                        <script>CKEDITOR.replace('body');</script>
                                        <span class="help-block">
                                            <strong>{{ $errors->first('body') }}</strong>
                                        </span>
                                    </div>
                                </div>
                                @if($dca->title=="CONTAINS")
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Foto</label>
                                    <input type="file" class="form-control" id="exampleInputPassword1" name="attribute" >
                                  </div>
                                @endif

                                @if($dca->title == "LASMIK LASER BIOREVITALISATION" || $dca->title == "LASMIK LASER ILBI")
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Youtube</label>
                                    <input type="text" class="form-control" id="exampleInputPassword1" name="attribute" value="{{$dca->attribute}}" >
                                  </div>
                                @endif
                                
                                
                                  <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@push('custom-scripts')
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('body', {
 filebrowserUploadUrl: "{{ route('posts.upload', ['_token' => csrf_token() ]) }}",
 filebrowserUploadMethod: 'form',
 toolbar: [
     { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
         { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
         { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
         { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
         '/',
         { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
         { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
         { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
         { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
         '/',
         { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
         { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
         { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
         { name: 'about', items: [ 'About' ] },
     ],
     allowedContent: true
 });
 </script>
@endpush

@endsection
