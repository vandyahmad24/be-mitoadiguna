@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Data Client</strong></div>
            <div class="card-body">
                <a href="{{route('client.create')}}" class="btn btn-primary">Tambah Client</a>
              <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Logo</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($client as $s)
                        <tr>
                            <td>{{$loop->iteration + $client->firstItem()-1}}</td>
                            <td>{{$s->name}}</td>
                            <td> <img src="{{asset('upload/client/')}}/{{$s->image}}" style='width:250px;height:auto;' border="0" alt="Null"></td>
                            <td>
                                <a href="{{route('client.edit',$s->id)}}" class="btn btn-success">Edit</a>
                                <form action="{{ route('client.destroy', $s->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this item?')">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-danger">Delete</button>
                              </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $client->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection