@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Data Demo</strong></div>
            <div class="card-body">
                <a href="{{route('booking.export')}}" class="btn btn-primary">Download Data</a>
              <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Email</th>
                        <th scope="col">No Hp</th>
                        <th scope="col">Klinik</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Jenis Alat</th>
                        <th scope="col">Jam</th>
                        <th scope="col">Created At</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($demo as $s)
                        <tr>
                            <td>{{$loop->iteration + $demo->firstItem()-1}}</td>
                            <td>{{$s->name}}</td>
                            <td>{{$s->email}}</td>
                            <td>{{$s->phone}}</td>
                            <td>{{$s->nama_klinik}}</td>
                            <td>{{$s->alamat}}</td>
                            <td>{{$s->tanggal}}</td>
                            <td>{{$s->jenis_alat}}</td>
                            <td>{{$s->jam}}</td>
                            <td>{{$s->created_at}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $demo->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection