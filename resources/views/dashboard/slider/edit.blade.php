@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>Edit Slider</strong></div>
                    <div class="card-body">
                        <div class="example">
                            <form method="POST" action="{{route('slider.update',$slider->id)}}" enctype='multipart/form-data'>
                               
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }} <br/>
                                    @endforeach
                                </div>
                                @endif
                
                                @csrf
                                @method("PUT")
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="title" id="floatingInput" placeholder="Title" value="{{$slider->title}}">
                                    <label for="floatingInput">Title</label>
                                </div>
                              

                                <div class="form-floating mb-3">
                                    <select class="form-select" name="type" id="type" aria-label="type">
                                      
                                        <option @if($slider->type=="home") selected @endif value="home">Home</option>
                                        <option @if($slider->type=="product") selected @endif value="product">Product</option>
                                        <option @if($slider->type=="news") selected @endif value="news">News</option>
                                      </select>
                                </div>

                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" id="floatingPassword"
                                        placeholder="Sub Title" name="subtitle" value="{{$slider->subtitle}}">
                                    <label for="floatingPassword" id="subtitle">Sub Title</label>
                                </div>
                              
                                <div class="mt-2">
                                    <label for="floatingPassword">Foto Sekarang</label>
                                    <br>
                                    <img id="image-preview"  src="{{asset('upload/slider/')}}/{{$slider->image}}" style='width:250px;height:auto;' border="0" alt="Null">
                                </div>
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Ganti Foto</label>
                                    <input type="file" class="form-control" id="exampleInputPassword1" name="image" >
                                  </div>

                                  <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@push('custom-scripts')
<script>
$(document).ready(function() {
    // Ketika pengguna memilih file baru
    $('input[type="file"]').change(function() {
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          // Mengubah atribut 'src' pada elemen gambar untuk menampilkan gambar baru
          $('#image-preview').attr('src', e.target.result);
        }
        // Membaca file yang dipilih oleh pengguna
        reader.readAsDataURL(this.files[0]);
      }
    });
  });

  $(document).ready(function() {
    $('#type').on('change', function() {
        if ($(this).val() === 'product') {
            // Jika tipe yang dipilih adalah 'product'
            $('input[name="subtitle"]').hide(); // Sembunyikan input sub title
            $("#subtitle").hide();
        } else {
            $('input[name="subtitle"]').show(); // Tampilkan input sub title untuk tipe lainnya
            $("#subtitle").show();
        }
    });
});
</script>
@endpush

@endsection
