@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Data Discus</strong></div>
            <div class="card-body">
              <a href="{{route('discus.export')}}" class="btn btn-primary">Download Data</a>
               <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Number</th>
                        <th scope="col">Country</th>
                        <th scope="col">Company Name</th>
                        <th scope="col">Interest At</th>
                        <th scope="col">Message</th>
                        <th scope="col">Discus At</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($discus as $s)
                        <tr>
                            <td>{{$loop->iteration + $discus->firstItem()-1}}</td>
                            <td>{{$s->name}}</td>
                            <td>{{$s->email}}</td>
                            <td>{{$s->number}}</td>
                            <td>{{$s->country}}</td>
                            <td>{{$s->company_name}}</td>
                            <td>{{$s->interested}}</td>
                            <td>{{$s->message}}</td>
                            <td>{{ \Carbon\Carbon::parse($s->created_at)->format('d-m-Y') }}
                            </td>
                            
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $discus->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection