@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>Tambah Slider</strong></div>
                    <div class="card-body">
                        <div class="example">
                            <form method="POST" action="{{route('slider.store')}}" enctype='multipart/form-data'>
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }} <br/>
                                    @endforeach
                                </div>
                                @endif
                
                                @csrf
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="title" id="floatingInput" placeholder="Title">
                                    <label for="floatingInput">Title</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <select class="form-select" name="type" id="type" aria-label="type">
                                        <option selected>Type</option>
                                        <option value="home">Home</option>
                                        <option value="product">Product</option>
                                        <option value="news">News</option>
                                      </select>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" id="floatingSubtitle"
                                        placeholder="Sub Title" name="subtitle">
                                    <label for="floatingSubtitle" id="subtitle">Subtitle</label>
                                </div>
                                
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Image</label>
                                    <input type="file" class="form-control" id="exampleInputPassword1" name="image" required>
                                  </div>
                                  <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@push('custom-scripts')
<script>
$(document).ready(function() {
    $('#type').on('change', function() {
        if ($(this).val() === 'product') {
            // Jika tipe yang dipilih adalah 'product'
            $('input[name="subtitle"]').hide(); // Sembunyikan input sub title
            $("#subtitle").hide();
        } else {
            $('input[name="subtitle"]').show(); // Tampilkan input sub title untuk tipe lainnya
            $("#subtitle").show();
        }
    });
});
</script>
@endpush



@endsection
