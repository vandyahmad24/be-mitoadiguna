@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Data Client Says</strong></div>
            <div class="card-body">
                {{-- <a href="{{route('book.create')}}" class="btn btn-primary">Tambah Buku/Jurnal</a> --}}
              <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Client Raing</th>
                        <th scope="col">Body</th>
                        <th scope="col">Name</th>
                        <th scope="col">Title</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($cs as $s)
                        <tr>
                            <td>{{$s->client_rating}}</td>
                            <td>{{$s->body}}</td>
                            <td>{{$s->name}}</td>
                            <td>{{$s->title}}</td>
                            <td> <img src="{{$s->photo}}" style='width:250px;height:auto;' border="0" alt="Null"></td>
                            <td>
                                <a href="{{route('client-says.edit',$s->id)}}" class="btn btn-success">Edit</a>
                                
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>



              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection