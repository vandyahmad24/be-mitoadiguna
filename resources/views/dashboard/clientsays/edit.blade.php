@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>Edit Client Says</strong></div>
                    <div class="card-body">
                        <div class="example">
                            <form method="POST" action="{{route('client-says.update',$cs->id)}}" enctype='multipart/form-data'>
                               
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }} <br/>
                                    @endforeach
                                </div>
                                @endif
                
                                @csrf
                                @method("PUT")
                                <div class="form-floating mb-3">
                                    <input type="number" class="form-control" name="client_rating" id="floatingInput" placeholder="client_rating" value="{{$cs->client_rating}}" >
                                    <label for="floatingInput">Client Rating</label>
                                </div>
                                
                              
                                <div class="mb-3">
                                    <label class="form-label" for="exampleFormControlTextarea1">Body</label>
                                    <textarea class="form-control" name="body" id="exampleFormControlTextarea1" rows="3">{{$cs->body}}</textarea>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="name" id="floatingInput" placeholder="name" value="{{$cs->name}}">
                                    <label for="floatingInput">Name</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="title" id="floatingInput" placeholder="title" value="{{$cs->title}}">
                                    <label for="floatingInput">Title</label>
                                </div>
                                
                                <div class="mt-2">
                                    <label for="floatingPassword">Photo</label>
                                    <br>
                                    <img id="image-preview"  src="{{$cs->photo}}" style='width:250px;height:auto;' border="0" alt="Null">
                                </div>
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Ganti Photo</label>
                                    <input type="file" class="form-control" id="exampleInputPassword1" name="photo" >
                                  </div>

                                  <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@push('custom-scripts')
<script>
$(document).ready(function() {
    // Ketika pengguna memilih file baru
    $('input[type="file"]').change(function() {
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          // Mengubah atribut 'src' pada elemen gambar untuk menampilkan gambar baru
          $('#image-preview').attr('src', e.target.result);
        }
        // Membaca file yang dipilih oleh pengguna
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
</script>
@endpush

@push('custom-scripts')
<script>
$(document).ready(function() {
  $("#exampleFormControlTextarea1").on("input", function() {
    if ($(this).val().length > 255) {
      alert("Teks tidak dapat lebih dari 255 karakter");
      $(this).val($(this).val().substring(0, 255));
    }
  });
});

$(document).ready(function() {
  $("#floatingInput").on("input", function() {
    if ($(this).val() > 5) {
        alert("Rating Maksimal 5");
      $(this).val(5);
    }
  });
});
</script>
@endpush

@endsection
