@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-sm-6 col-lg-3">
          <div class="card mb-4 text-white bg-primary">
            <div class="card-body pb-0 d-flex justify-content-between align-items-start">
              <div>
                <div class="fs-4 fw-semibold">Jumlah </div>
                <div>Pengunjung Hari ini</div>
              </div>
              
            </div>
            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
              <h1>{{$countToday}}</h1>
             </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="card mb-4 text-white bg-info">
            <div class="card-body pb-0 d-flex justify-content-between align-items-start">
              <div>
                <div class="fs-4 fw-semibold">Jumlah </div>
                <div>Pengunjung Bulan ini</div>
              </div>
              
            </div>
            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
              <h1>{{$countMonth}}</h1>
             </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="card mb-4 text-white bg-warning">
            <div class="card-body pb-0 d-flex justify-content-between align-items-start">
              <div>
                <div class="fs-4 fw-semibold">Jumlah </div>
                <div>Pembaca Artikel Hari ini</div>
              </div>
              
            </div>
            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
              <h1>{{$countArticleToday}}</h1>
             </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="card mb-4 text-white bg-danger">
            <div class="card-body pb-0 d-flex justify-content-between align-items-start">
              <div>
                <div class="fs-4 fw-semibold">Jumlah </div>
                <div>Pembaca Artikel Bulan ini</div>
              </div>
              
            </div>
            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
              <h1>{{$countArticleMonth}}</h1>
             </div>
          </div>
        </div>
       
      </div>
      <div class="card mt-5 mb-3 text-center">
        <br>
        <h5 class="">Pengunjung Harian Bulan  {{ date('F') }}</h5>
        <canvas id="dailyVisitorsChart"></canvas>
      </div>
    </div>
  </div>

@push('custom-scripts')
<script>
  $.ajax({
  url: "{{ route('visitor-api') }}",
  method: 'GET',
  dataType: 'json',
  success: function(data) {
    const labels = data.map(item => item.date);
    const values = data.map(item => item.total);
    const ctx = document.getElementById('dailyVisitorsChart').getContext('2d');
    const chart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: 'Daily Visitors',
          data: values,
          borderColor: 'rgb(75, 192, 192)',
          backgroundColor: 'rgba(75, 192, 192, 0.2)',
          tension: 0.4
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  },
  error: function(error) {
    console.error(error);
  }
});
</script>
@endpush

@endsection