@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>List Request Buku</strong></div>
            <div class="card-body">
              <a href="{{route('req-buku.export')}}" class="btn btn-primary">Download Data</a>
              <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">No Phone</th>
                        <th scope="col">Email</th>
                        <th scope="col">Pekerjaan</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($req as $s)
                        <tr>
                            <td>{{$loop->iteration + $req->firstItem()-1}}</td>
                            <td>{{$s->name}}</td>
                            <td>{{$s->phone}}</td>
                            <td>{{$s->email}}</td>
                            <td>{{$s->job}}</td>
                            
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $req->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection