@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Data Testimoni</strong></div>
            <div class="card-body">
                <a href="{{route('testimoni.create')}}" class="btn btn-primary">Tambah testimoni</a>
              <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                 
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Name</th>
                        <th scope="col">Role </th>
                        <th scope="col">Testimoni </th>

                        <th scope="col">Image</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($testimoni as $s)
                      
                        <tr>
                            <td>{{$loop->iteration + $testimoni->firstItem()-1}}</td>
                            <td>{{$s->name}}</td>
                            <td>{{$s->role}}</td>
                            <td>{{$s->testimoni}}</td>
                            <td> <img src="{{asset('upload/testimoni/')}}/{{$s->image}}" style='width:250px;height:auto;' border="0" alt="Null"></td>
                            <td>
                                <a href="{{route('testimoni.edit',$s->id)}}" class="btn btn-success">Edit</a>
                                <form action="{{ route('testimoni.destroy', $s->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this item?')">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-danger">Delete</button>
                              </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $testimoni->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection