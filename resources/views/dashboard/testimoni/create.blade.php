@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>Tambah Testimoni</strong></div>
                    <div class="card-body">
                        <div class="example">
                            <form method="POST" action="{{route('testimoni.store')}}" enctype='multipart/form-data'>
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }} <br/>
                                    @endforeach
                                </div>
                                @endif
                
                                @csrf
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="name" id="floatingInput" placeholder="Name">
                                    <label for="floatingInput">Name</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="role" id="floatingInput" placeholder="Role">
                                    <label for="floatingInput">Role</label>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="exampleFormControlTextarea1">Testimoni</label>
                                    <textarea class="form-control" name="testimony" id="exampleFormControlTextarea1" rows="3"></textarea>
                                  </div>
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Image</label>
                                    <input type="file" class="form-control" id="exampleInputPassword1" name="image" required>
                                  </div>
                                  <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@push('custom-scripts')
<script>
$(document).ready(function() {
  $("#exampleFormControlTextarea1").on("input", function() {
    if ($(this).val().length > 255) {
      alert("Teks tidak dapat lebih dari 255 karakter");
      $(this).val($(this).val().substring(0, 255));
    }
  });
});
</script>
@endpush
@endsection
