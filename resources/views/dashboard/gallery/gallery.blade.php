@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header"><strong>Data Gallery</strong></div>
            <div class="card-body">
                {{-- <a href="{{route('gallery.create')}}" class="btn btn-primary">Tambah Gallery</a> --}}
              <div class="example">
                
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
                <div class="table-responsive">
<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Posisi</th>
                        <th scope="col">Image</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($gallery as $s)
                        <tr>
                            <td>{{$loop->iteration + $gallery->firstItem()-1}}</td>
                            <td>{{$s->posisi}}</td>
                            <td> <img src="{{asset('upload/gallery/')}}/{{$s->image}}" style='width:250px;height:auto;' border="0" alt="Null"></td>
                            <td>
                                <a href="{{route('gallery.edit',$s->id)}}" class="btn btn-success">Edit</a>
                               
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
</div>

                  {{ $gallery->links('vendor.pagination.bootstrap-4') }}


              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection