@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>Tambah Gallery</strong></div>
                    <div class="card-body">
                        <div class="example">
                            <form method="POST" action="{{route('gallery.store')}}" enctype='multipart/form-data'>
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }} <br/>
                                    @endforeach
                                </div>
                                @endif
                
                                @csrf
                                <div class="form-floating">
                                    <select class="form-select" name="posisi" aria-label="Posisi">
                                        <option selected>Posisi</option>
                                        <option value="center">Tengah</option>
                                        <option value="right">Kanan</option>
                                        <option value="left">Kiri</option>
                                      </select>
                                </div>
                                <div class="mt-2">
                                    <label for="exampleInputPassword1" class="form-label">Image</label>
                                    <input type="file" class="form-control" id="exampleInputPassword1" name="image" required>
                                  </div>
                                  <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
