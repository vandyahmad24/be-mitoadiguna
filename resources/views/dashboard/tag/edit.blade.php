@extends('template.dashboard_template')

@section('content')

<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header"><strong>Edit Tag</strong></div>
                    <div class="card-body">
                        <div class="example">
                            <form method="POST" action="{{route('tag.update',$tag->id)}}" enctype='multipart/form-data'>
                               
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    {{ $error }} <br/>
                                    @endforeach
                                </div>
                                @endif
                
                                @csrf
                                @method("PUT")
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="name" id="floatingInput" placeholder="Title" value="{{$tag->name}}">
                                    <label for="floatingInput">Nama Tag</label>
                                </div>
                              
                              
                               

                                  <div class="mt-2">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


@endsection
