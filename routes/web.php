<?php

use App\Http\Controllers\ApiInstagramController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CkeditorController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ClientSaysController;
use App\Http\Controllers\ClinicalApplicationController;
use App\Http\Controllers\CountController;
use App\Http\Controllers\DemoController;
use App\Http\Controllers\DiscusUsController;
use App\Http\Controllers\GalleryController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PhotoProductController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RequestBookListController;
use App\Http\Controllers\SliderContactUsController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\SliderProductController;
use App\Http\Controllers\SubscribeController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TestimonyController;
use App\Http\Controllers\YoutubeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect()->route('login');
});



Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...  
]);
Route::middleware(['auth'])->group(function () {
    Route::resource('/posts', PostController::class);
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::resource('slider', SliderController::class);
    Route::resource('slider-contact-us', SliderContactUsController::class);
    Route::resource('slider-product', SliderProductController::class);
    Route::resource('booking-demo',DemoController::class);
    Route::get('booking-demo-download',[DemoController::class,'export'])->name('booking.export');
    Route::get('discus-download',[DiscusUsController::class,'export'])->name('discus.export');
    Route::get('subscribe-download',[SubscribeController::class,'export'])->name('subscribe.export');
    Route::resource('client',ClientController::class);
    Route::resource('testimoni',TestimonyController::class);
    Route::resource('team',TeamController::class);
    Route::resource('gallery',GalleryController::class);
    Route::resource('book',BookController::class);
    Route::resource('categories',CategoryController::class);
    Route::resource('tag',TagController::class);
    Route::put('/posts/{post}/publish', [PostController::class,'publish'])->name('posts.publish');
    Route::post('ckeditor/image_upload', [CkeditorController::class, 'upload'])->name('posts.upload');
    Route::get('/subscribe',[SubscribeController::class,'index'])->name('subscribe.index');
    Route::get('/discus',[DiscusUsController::class,'index'])->name('discus.index');
    Route::get('/product/{id}',[ProductController::class,'index'])->name('product.menu');
    Route::resource('photo-product',PhotoProductController::class);
    route::get('/clinical-application/{product_id}',[ClinicalApplicationController::class,'index'])->name('clinical_application.index');
    route::get('/clinical-application-detail/{product_id}/{id}',[ClinicalApplicationController::class,'detail'])->name('clinical_application.detail');
    route::get('/clinical-application-edit/{product_id}/{id}/{detail_id}',[ClinicalApplicationController::class,'edit'])->name('clinical_application.edit');
    route::put('/clinical-application-edit/{product_id}/{id}/{detail_id}',[ClinicalApplicationController::class,'update'])->name('clinical_application.put');
    Route::get('/youtube',[YoutubeController::class,'index'])->name('youtube.index');
    Route::put('/youtube',[YoutubeController::class,'update'])->name('youtube.update');
    Route::get('/request-buku',[RequestBookListController::class,'list'])->name('req-buku.index');
    Route::get('/request-buku-export',[RequestBookListController::class,'export'])->name('req-buku.export');
    Route::resource('client-says',ClientSaysController::class);
    
});

Route::get('/visitor', [CountController::class,'grafik'])->name('visitor-api');