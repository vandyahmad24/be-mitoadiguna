<?php

use App\Helpers\ResponseHelper;
use App\Http\Controllers\ApiInstagramController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ClientSaysController;
use App\Http\Controllers\ClinicalApplicationController;
use App\Http\Controllers\CountController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DemoController;
use App\Http\Controllers\DiscusUsController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\PhotoProductController;
use App\Http\Controllers\PostApiController;
use App\Http\Controllers\RequestBookListController;
use App\Http\Controllers\SliderContactUsController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\SliderProductController;
use App\Http\Controllers\SubscribeController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TestimonyController;
use App\Http\Controllers\YoutubeController;
use Spatie\FlareClient\Api;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::GET('healtz',[ApiInstagramController::class,'updateIg']);

Route::middleware(['hitCounter','corsHandle'])->group(function () {
    Route::post('booking-demo',[DemoController::class,'createApi']);
    Route::get('/slider', [SliderController::class,'listSlider']);
    Route::get('/client', [ClientController::class,'listClient']);
    Route::get('/testimoni', [TestimonyController::class,'listTestimoni']);
    Route::get('/team', [TeamController::class,'listTeam']);
    Route::get('/gallery', [GalleryController::class,'listGallery']);
    Route::get('/book', [BookController::class,'listBook']);
    Route::get('/posts', [PostApiController::class,'listApi']);
    Route::post('/book-list', [RequestBookListController::class,'create']);


    Route::get('/tag', [TagController::class,'list']);
    Route::get('/category', [CategoryController::class,'list']);
    Route::post('/subscribe', [SubscribeController::class,'post']);
    Route::post('/discus-us', [DiscusUsController::class,'post']);
    Route::get('/photo-product/{id}', [PhotoProductController::class,'listProduct']);
    Route::get('/instagram', [ApiInstagramController::class,'listInstagram']);
    Route::get('/instagram-foto/{id}', [ApiInstagramController::class,'getImage']);
    Route::get('/get-clinical-application/{productId}', [ClinicalApplicationController::class,'getListCA']);
    Route::get('/get-clinical-application/{productId}/{CaId}', [ClinicalApplicationController::class,'getListCAById']);
    Route::get('/youtube', [YoutubeController::class,'getApi']);
    Route::get('/slider-product', [SliderProductController::class,'listSlider']);
    Route::get('/banner-contact', [SliderContactUsController::class,'listSlider']);
    Route::get('/client-says', [ClientSaysController::class,'listClient']);
});
Route::get('/posts/{id}', [PostApiController::class,'show'])->middleware('articleCounter','corsHandle');


